$(function(){
    /*this file should be minified prior to deployment*/
    
    /*global $*/
    /*global exit*/
    /*global CryptoJS*/ //the CryptoJS needs to be in the page header
    
    $('#demoSubmit').click(function(event){
        event.preventDefault();
        SaveDemo('updateProcess');
    });
    
});

function HashString(thisString){
    //method to use exact same hash algorithm for all strings
    var tempString = thisString;
    
    //apply hash against 'tempString' to maintain integrity of original string
    
    tempString = CryptoJS.MD5(tempString).toString()//simple MD5 hash for demo purposes
    
    return tempString;
}

function ResetLoginMessages(){
    //remove all class info
    $('#login_message_container').removeClass('alert-danger');
    $('#login_message_container').removeClass('alert-success');
    $('#login_message_container').removeClass('alert-warning');
    $('#login_message_container').removeClass('alert');
    $('#login_message_container').addClass('no_display');
    $('#login_message').html('');
}

function ValidateLoginCredentials(loginid, loginpass){
    if (loginid.length==0) {
        $('#login_message_container').addClass('alert alert-warning');
        $('#login_message_container').removeClass('no_display');
        document.getElementById('login_message').innerHTML = '<center>Login E-Mail cannot be blank</center>';
        return false;
    }else if(loginpass.length==0) {
        $('#login_message_container').addClass('alert alert-warning');
        $('#login_message_container').removeClass('no_display');
        document.getElementById('login_message').innerHTML = '<center>Password cannot be blank</center>';
        return false;
    }else{
        return true;
    }
}

function processLogin(originRequest){
    ResetLoginMessages();
    
    var loginid = $('#loginemail').val();
    var pass = $('#loginpassword').val();
    
    if(!ValidateLoginCredentials(loginid, pass)){
        return;    
    }
    
    pass = HashString(pass);
    
    $.post('databridge.php',{
        req:"loginProcess",
        id:loginid,
        password:pass,
        origin:originRequest,
        token:$('#'+originRequest+'token_beta').val(),
        alphatoken:$('#validation_token_alpha').val()
    }, function(data){
        //console.log(data);
        var obj = $.parseJSON(data);
        //console.log(obj);
        
        if(obj['results']['status']=='success'){
            document.getElementById('login_message').innerHTML = 'Login Successful';
            $('#login_message_container').removeClass('no_display');
            $('#login_message_container').addClass('alert alert-success');
            
            //no need to update tokens here since we are reloading
            
            setTimeout(function(){
               window.location.reload(1);
            }, 500);
            
        }else{
            document.getElementById('login_message').innerHTML = obj['results']['errorMessage'];
            $('#login_message_container').removeClass('no_display');
            $('#login_message_container').addClass('alert alert-danger');
            
            //reset validation tokens
            if(typeof obj['results']['betatoken'] === 'undefined') {
                //do nothing
            }else{
                $('#'+originRequest+'token_beta').val(obj['results']['betatoken']);    
            }
            
            if(typeof obj['results']['alphatoken'] === 'undefined') {
                //do nothing
            }else{
                $('#validation_token_alpha').val(obj['results']['alphatoken']);    
            }
           
        }
          
    });//post
    
}

function RefreshDemographics(userid, originRequest){
    $.get('databridge.php',{
        req:"refreshDemo",
        id:userid,
        origin:originRequest,
        token:$('#'+originRequest+'token_beta').val(),
        alphatoken:$('#validation_token_alpha').val()
    },function(data){
        var obj = $.parseJSON(data);
        
        
        //reset validation tokens
        if(typeof obj['results']['betatoken'] === 'undefined') {
            //do nothing
        }else{
            $('#'+originRequest+'token_beta').val(obj['results']['betatoken']);    
        }
        
        if(typeof obj['results']['alphatoken'] === 'undefined') {
            //do nothing
        }else{
            $('#validation_token_alpha').val(obj['results']['alphatoken']);    
        }
        
        if(obj['results']['status']=='success'){
        
            document.getElementById('loggedinas').innerHTML = obj['demographics']['firstname']+' '+obj['demographics']['lastname'];
            document.getElementById('demopreview_firstname').innerHTML = obj['demographics']['firstname'];
            document.getElementById('demopreview_lastname').innerHTML = obj['demographics']['lastname'];
            document.getElementById('demopreview_email').innerHTML = obj['demographics']['email'];
            document.getElementById('demopreview_dob').innerHTML = obj['demographics']['dob'];
            document.getElementById('demopreview_address').innerHTML = obj['demographics']['address']+', '+obj['demographics']['address2'];
            document.getElementById('demopreview_city').innerHTML = obj['demographics']['city'];
            document.getElementById('demopreview_statezip').innerHTML = obj['demographics']['state']+', '+obj['demographics']['zip'];
            document.getElementById('demopreview_country').innerHTML = obj['demographics']['country'];
            
            var dob = obj['demographics']['dob'].slice(0,10);//should use moment.js to turn this to a correct date
            
            $('#editdemo_firstname').val(obj['demographics']['firstname']);
            $('#editdemo_lastname').val(obj['demographics']['lastname']);
            $('#editdemo_email').val(obj['demographics']['email']);
            document.getElementById('editdemo_dob').value = dob;
            $('#editdemo_address').val(obj['demographics']['address']);
            $('#editdemo_city').val(obj['demographics']['city']);
            $('#editdemo_zip').val(obj['demographics']['zip']);
            
            
        }else{
            document.getElementById('loggedinas').innerHTML = 'failure';   
            console.log(obj);
        }
        
        document.getElementById('profileNav').removeAttribute('style');//dependent on latency, this might need to be a promise
    });
    
}

function ResetSaveDemoMessages(){
    $('#profile_message_container').removeClass('alert-danger');
    $('#profile_message_container').removeClass('alert-success');
    $('#profile_message_container').removeClass('alert-warning');
    $('#profile_message_container').removeClass('alert');
    $('#profile_message_container').addClass('no_display');
    $('#profile_message').html('');    
}

function SaveDemo(originRequest){
    ResetSaveDemoMessages();
    
    //add validation
   
   $.post('databridge.php',{
        req:"demoUpdate",
        userkey:$('#userToken').val(),
        firstname:$('#editdemo_firstname').val(),
        lastname:$('#editdemo_lastname').val(),
        dateofbirth:$('#editdemo_dob').val(),
        email:$('#editdemo_email').val(),
        address:$('#editdemo_address').val(),
        city:$('#editdemo_city').val(),
        zip:$('#editdemo_zip').val(),
        country:$('#editdemo_lastname').val(),
        origin:originRequest,
        token:$('#'+originRequest+'token_beta').val(),
        alphatoken:$('#validation_token_alpha').val()
    }, function(data){
        console.log(data);
        var obj = $.parseJSON(data);
        
        //reset validation tokens
        if(typeof obj['results']['betatoken'] === 'undefined') {
            //do nothing
        }else{
            $('#'+originRequest+'token_beta').val(obj['results']['betatoken']);    
        }
        
        if(typeof obj['results']['alphatoken'] === 'undefined') {
            //do nothing
        }else{
            $('#validation_token_alpha').val(obj['results']['alphatoken']);    
        }
        
        if(obj['results']['status']=='success'){
            document.getElementById('profile_message').innerHTML = 'Demographic information successfully updated';
            $('#profile_message_container').removeClass('no_display');
            $('#profile_message_container').addClass('alert alert-success');
            
            //no need to update tokens here since we are reloading
            
            setTimeout(function(){
               window.location.reload(1);
            }, 500);
            
        }else{
            document.getElementById('profile_message').innerHTML = obj['results']['errorMessage'];
            $('#profile_message_container').removeClass('no_display');
            $('#profile_message_container').addClass('alert alert-danger');
        }
        
    });
}

function Logout(){
    window.location = 'logout.php';
}

function reload(){
    window.location = '/';
}