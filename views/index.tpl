<!DOCTYPE html>
<html>
    <head>
        {$headers}
    </head>
    <body>
        <input type="hidden" id="validation_token_alpha" value="{$session_id}" />
        {include file='jumbotron_header.tpl'}
        
        <div id="main_container">
        {if isset($smarty.session.isloggedin) && $smarty.session.isloggedin=='yes'}
            {include file='profile.tpl'}
            {$updateToken}
            <script>RefreshDemographics('{$smarty.session.userkey}','updateProcess');</script>
        {else}
            {include file='login.tpl'}
        {/if}
        </div>
        
        {include file='footer.tpl'}
    </body>
</html>