{include file="navbar.tpl"}
<div class="container">
  
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              

      <div class="text-center">
        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar">
        <!--<h6>Upload a different photo...</h6>
        <input type="file" class="text-center center-block file-upload">-->
      </div></hr><br>

               
          <div class="panel panel-default">
            <div class="panel-heading">Website <i class="fa fa-link fa-1x"></i></div>
            <div class="panel-body"><a href="https://web-samples-connorlync.c9users.io/">https://web-samples-connorlync.c9users.io/</a></div>
          </div>
               
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#preview">Preview</a></li>
                <li><a data-toggle="tab" href="#edit">Edit</a></li>
              </ul>

              
          <div class="tab-content">
            <div class="tab-pane active" id="preview">
                <hr>
                    <form class="form" action="##" method="post" id="registrationForm">
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="first_name"><h4>First name</h4></label>
                              <br/><span id="demopreview_firstname"></span>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="last_name"><h4>Last name</h4></label>
                              <br/><span id="demopreview_lastname"></span>
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>Email</h4></label>
                              <br/><span id="demopreview_email"></span>
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="dob"><h4>DOB</h4></label>
                              <br/><span id="demopreview_dob"></span>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="address"><h4>Address</h4></label>
                              <br/><span id="demopreview_address"></span>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="city"><h4>City</h4></label>
                              <br/><span id="demopreview_city"></span>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="state"><h4>State/Zip</h4></label>
                              <br/><span id="demopreview_statezip"></span>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="country"><h4>Country</h4></label>
                             <br/><span id="demopreview_country"></span>
                          </div>
                      </div>
                      
              	</form>
              
              <hr>
              
             </div><!--/tab-pane-->
             <div class="tab-pane" id="edit">
               
               <h2></h2>
               
               <hr>
                  <form class="form" action="##" method="post" id="registrationForm">
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="editdemo_firstname"><h4>First name</h4></label>
                              <input type="text" class="form-control" name="editdemo_firstname" id="editdemo_firstname" placeholder="first name" title="enter your first name if any.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="editdemo_lastname"><h4>Last name</h4></label>
                              <input type="text" class="form-control" name="editdemo_lastname" id="editdemo_lastname" placeholder="last name" title="enter your last name if any.">
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="editdemo_email"><h4>Email</h4></label>
                              <input type="email" class="form-control" name="editdemo_email" id="editdemo_email" placeholder="enter email" title="enter your email if any.">
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="editdemo_dob"><h4>DOB</h4></label>
                              <input type="date" class="form-control" name="editdemo_dob" id="editdemo_dob" placeholder="date of birth" title="enter your date of birth.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="editdemo_address"><h4>Address</h4></label>
                              <input type="text" class="form-control" name="editdemo_address" id="editdemo_address" placeholder="address" title="enter your address.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="editdemo_city"><h4>City</h4></label>
                              <input type="text" class="form-control" name="editdemo_city" id="editdemo_city" placeholder="somewhere" title="enter a city">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="editdemo_zip"><h4>Zip</h4></label>
                              <input type="text" class="form-control" name="editdemo_zip" id="editdemo_zip" placeholder="zip-code" title="enter your zip-code">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="editdemo_country"><h4>Country</h4></label><br/>
                              <select name="editdemo_country" id="editdemo_country">
                                  <option>United States of America</option>
                                  <option>Anywhere Else</option>
                              </select>
                          </div>
                      </div>
                      <div class="form-group pull-right">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-default" type="submit" id="demoSubmit">Save</button>
                               	<button class="btn" type="button" onclick="reload();">Cancel</button>
                               	<input type="hidden" id="userToken" value="{$smarty.session.userkey}"/>
                            </div>
                      </div>
              	</form>
               
             </div><!--/tab-pane-->
             
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
    <div class="row">
        <div id="profile_message_container" class="">
            <span id="profile_message"></span>
        </div>
    </div>
</div>