<?php
/*============  Basic Setup =============*/
session_start();
date_default_timezone_set('America/New_York');

/*============  Library Files =============*/
require_once ('inc/objlib.php');
require_once('Smarty/libs/Smarty.class.php');

/*============  Smarty Template Setup =============*/

$smarty=new Smarty();
$smarty->template_dir ='views';
$smarty->compile_dir = 'tmp';

$smarty->assign('session_id', session_id());

/*============  Output standard headers =============*/

$headers = "
<meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\"/>
<title>PHP Sample Application</title>
<meta name=\"viewport\" content=\"width=device-width, minimum-scale=1.0, maximum-scale=1.0\"/>
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
<meta HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"NO-CACHE\">
<meta HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">
<meta HTTP-EQUIV=\"expires\" CONTENT=\"0\">
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/md5.js\"></script>
<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\">
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>
<script src=\"inc/custom.js\"></script>
<link rel=\"stylesheet\" href=\"css/default.css\">";

$smarty->assign('headers',$headers);
?>