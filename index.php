<?php
    require_once('headers.php');
    
    if((array_key_exists('isloggedin',$_SESSION))&&($_SESSION['isloggedin']=='yes')){
        
        //client has logged in, set up different 'things'
        $updateProcess=csrfguard_inject('updateProcess',$updateTokenNew);
        
        $smarty->assign('updateToken',$updateProcess);
        
    }else{
        
        $logintoken=csrfguard_inject('loginProcess',$updateTokenNew);
    
        $smarty->assign('logintoken',$logintoken);    
    }
    
    $smarty->display('index.tpl');
?>