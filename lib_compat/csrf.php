<?php

    //the 'lib_compat/random.php' files are required for random 32 bit string
    //generation that was not offered on versions of php prior to 7.0

    require_once "random.php";
    
    if(!function_exists('hash_equals'))//not required if on php5.7+
    {
        function hash_equals($str1, $str2)
        {
            if(strlen($str1) != strlen($str2))
            {
                return false;
            }
            else
            {
                $res = $str1 ^ $str2;
                $ret = 0;
                for($i = strlen($res) - 1; $i >= 0; $i--)
                {
                    $ret |= ord($res[$i]);
                }
                return !$ret;
            }
        }
    }

    function store_in_session($key,$value){
        if (isset($_SESSION))
        {
            unset_session($key);
        	$_SESSION[$key]=$value;
        }else{
            session_start();
            $_SESSION[$key]=$value;
        }
    }//StoreInSession
    
    function unset_session($key)
    {
        $_SESSION[$key]=' ';
        unset($_SESSION[$key]);
    }//unset_session
    
    function get_from_session($key)
    {
        if (isset($_SESSION[$key]))
        {
        	return $_SESSION[$key];
        }
        else {  return false; }
    }//get_from_session
    
    function generate_csrf_token($unique_container_name){
        $token = random_bytes(32); // via paragonie/random_compat
        /*
        //remove single and double quotes
        $token = str_replace('"',"",$token);
        $token = str_replace("'",'"',$token);*/
        
        $token = bin2hex($token);
        
    	store_in_session($unique_container_name,$token);
    	return $token;    
    }
    
    function csrfguard_validate_token($unique_container_name,$token_value, &$newToken)
    {
    	$token = get_from_session($unique_container_name);
    	if (!is_string($token_value)) {
            $result = false;
    	}else{
    	    $result = hash_equals($token, $token_value);
    	}
    	
    	unset_session($unique_container_name);
    	$newToken = generate_csrf_token($unique_container_name);
    	
    	return $result;
    }
    
    function csrfguard_inject($unique_container_name, &$rtntoken)
    {
        $token=generate_csrf_token($unique_container_name);
        
        $name=$unique_container_name;
        $rtntoken = $token;
        
        $data_html="<input type='hidden' id='{$name}token_beta' value='{$token}' />";
        
        return $data_html;
    }

?>