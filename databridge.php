<?php

    session_start();

    require_once('inc/objlib.php');
    
    //this validation should really be moved to a separate file!!!
    
    //validate the request is coming from a valid session
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if((!isset($_POST['alphatoken']))||($_POST['alphatoken']!=session_id())){
            unset($_POST);
            
            $response['results'] = array(
            'status' => 'failure',
            'errorMessage' => 'Unrecognized Request - Cannot Continue');  
            
            $encodedResults= json_encode($response); 
            exit($encodedResults);   
            break;
        } 
    }elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
        if((!isset($_GET['alphatoken']))||($_GET['alphatoken']!=session_id())){
            unset($_GET);
            
            $response['results'] = array(
            'status' => 'failure',
            'errorMessage' => 'Unrecognized Request - Cannot Continue');  
            
            $encodedResults= json_encode($response); 
            exit($encodedResults);   
            break;
        }      
    }else{
        $response['results'] = array(
        'status' => 'failure',
        'errorMessage' => 'Unrecognized Request - Please Contact Technical Support');  
        
        $encodedResults= json_encode($response); 
        exit($encodedResults);   
        break;    
    }


    //validate the origin / referer as part of CSRF check
    
    if(array_key_exists('HTTP_HOST',$_SERVER)){$http_host = $_SERVER['HTTP_HOST'];}else{$http_host = '';}
    if(array_key_exists('HTTP_ORIGIN',$_SERVER)){$http_origin = $_SERVER['HTTP_ORIGIN'];}else{$http_origin = '';}
    if(array_key_exists('HTTP_REFERER',$_SERVER)){$http_referrer = $_SERVER['HTTP_REFERER'];}else{$http_referrer = '';}
    
    $pattern = array();
    $pattern[0] = '|http:|';
    $pattern[1] = '|https:|';
    $pattern[2] = '|//|';
    
    $http_host=preg_replace($pattern,'',$http_host);
    $http_origin=preg_replace($pattern,'',$http_origin);
    $http_referrer=preg_replace($pattern,'',$http_referrer);
    $n=0;
    $n = strlen($http_host);
    $http_referrer = substr($http_referrer,0,$n);
    
    //splitting the validation up so it is easier to read
    if((($http_origin=='null')||($http_origin==''))&&(($http_referrer=='null')||($http_referrer==''))){
        $response['results'] = array(
        'status' => 'failure',
        'errorMessage' => 'Unrecognized Request 1');  
        
        $encodedResults= json_encode($response); 
        exit($encodedResults);    
    }elseif(($http_origin<>'')&&($http_origin<>'null')){
        if(strpos($http_host,$http_origin)===false){
            //http_response_code(400);
            
            $response['results'] = array(
            'status' => 'failure',
            'errorMessage' => 'Unrecognized Request 2');   
            
            $encodedResults= json_encode($response); 
            exit($encodedResults);
        }
    }elseif(($http_referrer<>'')&&($http_referrer<>'null')){
        if(strpos($http_host,$http_referrer)===false){
            //http_response_code(400);
            
            $response['results'] = array(
            'status' => 'failure',
            'errorMessage' => 'Unrecognized Request 3');    
            
            $encodedResults= json_encode($response); 
            exit($encodedResults);
        }    
    }
    
    //check that the CSRF token that was passed in was the correct one for this session
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
        if(!array_key_exists('token',$_POST)){  
            //no CSRF token, do NOT continue processing!!
            
            $response['results'] = array(
            'status' => 'failure',
            'errorMessage' => 'Unrecognized Request 4');    
            
            //print_r($_POST);
            
            $encodedResults= json_encode($response); 
            exit($encodedResults);
            break;
        }else{
            $result = csrfguard_validate_token($_POST['origin'], $_POST['token'], $newToken);
            
            if(!$result){
                $response['results'] = array(
                'status' => 'failure',
                'errorMessage' => 'Could not validate request, please try again',
                'refreshAlphaToken' => $newToken);  
                //print_r($_POST);
                $encodedResults= json_encode($response); 
                exit($encodedResults);
            }
        }
    }elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
        //we do not REALLY need to validate for get requests
        //skipping implementation for now
        if(!array_key_exists('token',$_GET)){  
            //no CSRF token, do NOT continue processing!!
            
            $response['results'] = array(
            'status' => 'failure',
            'errorMessage' => 'Unrecognized Request 5');    
            
            //print_r($_POST);
            
            $encodedResults= json_encode($response); 
            exit($encodedResults);
            break;
        }else{
            $result = csrfguard_validate_token($_GET['origin'], $_GET['token'], $newToken);
            
            if(!$result){
                $response['results'] = array(
                'status' => 'failure',
                'errorMessage' => 'Could not validate request, please try again',
                'refreshAlphaToken' => $newToken);  
                //print_r($_POST);
                $encodedResults= json_encode($response); 
                exit($encodedResults);
            }
        }
    }
    
    //mow that the request has been validated as being a valid request from this sever,
    //go ahead and process the request
    
    $dataObj = new WebSample();
    
    if (($_SERVER['REQUEST_METHOD'] === 'POST')&&(array_key_exists('req',$_POST))) {
        switch($_POST['req']){
            case 'loginProcess':
                $result = $dataObj->Login($_POST['id'],$_POST['password'],$rtnMsg);
                //because we attempted a login, the session will have regenerated, add it to the return array
                if($result['result']==='SUCCESS'){
                    $response['results'] = array(
                        'status' => 'success',
                        'errorMessage' => '',
                        'betatoken' => $newToken,
                        'alphatoken' => session_id()); 
                     
                }else{
                    $response['results'] = array(
                        'status' => 'failure',
                        'errorMessage' => $rtnMsg,
                        'betatoken' => $newToken,
                        'alphatoken' => session_id());      
                }
                
                $encodedResults= json_encode($response); 
                exit($encodedResults);
                
                break;
                
            case 'demoUpdate':
                $result = $dataObj->UpdateDemographics($_POST['userkey'],$_POST['firstname'],
                $_POST['lastname'],$_POST['email'],$_POST['dateofbirth'],$_POST['address'],
                $_POST['city'],$_POST['zip'],$_POST['country'],$rtnMsg);
                
                if($result['result']==='SUCCESS'){
                    $response['results'] = array(
                        'status' => 'success',
                        'errorMessage' => '',
                        'betatoken' => $newToken,
                        'alphatoken' => session_id()); 
                     
                }else{
                    $response['results'] = array(
                        'status' => 'failure',
                        'errorMessage' => $rtnMsg,
                        'betatoken' => $newToken,
                        'alphatoken' => session_id());      
                }
                
                $encodedResults= json_encode($response); 
                exit($encodedResults);
                
                break;
            default:
                $response['results'] = array(
                'status' => 'failure',
                'errorMessage' => 'Unrecognized request, please try again',
                'refreshAlphaToken' => $newToken);  
                
                $encodedResults= json_encode($response); 
                exit($encodedResults);    
        }
    }elseif (($_SERVER['REQUEST_METHOD'] === 'GET')&&(array_key_exists('req',$_GET))) {
        switch($_GET['req']){
            case 'refreshDemo':
                $result=$dataObj->GetDemographics($_GET['id'],$rtnMsg);
                
                if($result['result']==='SUCCESS'){
                    $response['results'] = array(
                        'status' => 'success',
                        'errorMessage' => '',
                        'betatoken' => $newToken,
                        'alphatoken' => session_id()); 
                    $response['demographics']=$result['demographics'];
                     
                }else{
                    $response['results'] = array(
                        'status' => 'failure',
                        'errorMessage' => $rtnMsg,
                        'betatoken' => $newToken,
                        'alphatoken' => session_id());      
                }
                
                $encodedResults= json_encode($response); 
                exit($encodedResults);
                
                break;
            default:
                $response['results'] = array(
                'status' => 'failure',
                'errorMessage' => 'Unrecognized request, please try again'.$_GET['req'],
                'refreshAlphaToken' => $newToken);  
                
                $encodedResults= json_encode($response); 
                exit($encodedResults);    
        }
    }else{
        //default response, not sure how we can really get here
        $response['results'] = array(
            'status' => 'failure',
            'errorMessage' => 'Could not process request');  
            
            $encodedResults= json_encode($response); 
            exit($encodedResults);
    }
?>