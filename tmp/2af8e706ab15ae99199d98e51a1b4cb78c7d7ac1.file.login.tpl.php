<?php /* Smarty version Smarty-3.1.15, created on 2018-08-09 22:29:49
         compiled from "views/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1319390145b6cdb41bbedc3-52062018%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2af8e706ab15ae99199d98e51a1b4cb78c7d7ac1' => 
    array (
      0 => 'views/login.tpl',
      1 => 1533868162,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1319390145b6cdb41bbedc3-52062018',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5b6cdb41bc0004_68573609',
  'variables' => 
  array (
    'logintoken' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b6cdb41bc0004_68573609')) {function content_5b6cdb41bc0004_68573609($_smarty_tpl) {?><div class="container login_panel">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Login</h3>
        </div>
        <div style="padding-top:30px" class="panel-body">
           
            <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="loginemail" type="text" class="form-control" name="username" value="" placeholder="E-Mail"  autocomplete="off">                                        
            </div>
                
            <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input id="loginpassword" type="password" class="form-control" name="password" placeholder="Password"  autocomplete="off">
            </div> 
            
            <div class="form-group" id="login_process">
                <div class="col-sm-12 controls">
                    <input type="button" class="btn btn-primary panel_login_button pull-right" value="SIGN IN" onclick="processLogin('loginProcess');" /> 
                </div>
            </div>
            <?php echo $_smarty_tpl->tpl_vars['logintoken']->value;?>

            <div class="clear"><br/><br/></div>
            <div id="login_message_container" class="panel_feedback" >
                <span id="login_message"></span>
            </div>
            <div class="clear"><br/><br/></div>
            <div>
                <span>Demo Email: connor.brannigan@gmail.com</span><br/>
                <span>Demo Pass: WebSampl3</span>
            </div>
        </div>
    </div>
</div><?php }} ?>
