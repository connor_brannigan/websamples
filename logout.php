<?php
    session_start();
    session_unset();
    session_destroy();
    unset($_GET);
    unset($_POST);
    
    header("location:/");
?>
