create database websamples;
grant select, insert, update, delete on websamples.* to websamples@localhost identified by 'websamples101';
flush privileges;

use websamples;

Create Table: CREATE TABLE `useractivations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` varchar(100) DEFAULT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userstatus` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

CREATE TABLE `userdemographics` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` varchar(100) NOT NULL,
  `firstname` varchar(35) DEFAULT NULL,
  `middlename` varchar(35) DEFAULT NULL,
  `lastname` varchar(35) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `dob` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `address` varchar(100) DEFAULT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `county` varchar(50) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_userid` (`userid`)
);

CREATE TABLE `userlogins` (
  `loginid` int(10) NOT NULL AUTO_INCREMENT,
  `loginemail` varchar(50) NOT NULL,
  `loginpassword` varchar(100) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastlogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userid` varchar(100) NOT NULL,
  PRIMARY KEY (`loginid`)
);

CREATE TABLE `userstatus` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `description_en` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO USERACTIVATIONS(USERID)VALUES('d8d44f2e86abced5eec0fee004805538');
INSERT INTO USERSTATUS(DESCRIPTION_EN)VALUES('Active');
INSERT INTO USERLOGINS(loginemail, loginpassword, userid)VALUES('connor.brannigan@gmail.com','d8d44f2e86abced5eec0fee004805538','d8d44f2e86abced5eec0fee004805538');
INSERT INTO USERDEMOGRAPHICS(userid,firstname, lastname, email,dob,address,city,county,state,zip,country)VALUES('d8d44f2e86abced5eec0fee004805538','Connor','Brannigan','connor.brannigan@gmail.com','1980-01-01','Tifft St','Buffalo','Erie','NY','Zip','United States of America');