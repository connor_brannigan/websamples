<nav id="profileNav" class="navbar navbar-default" style="display:none">
  <div class="container-fluid">
    <div class="navbar-header">
      <span class="navbar-text">Logged in as <span id="loggedinas"></span></span>
    </div>
    <button class="btn btn-default navbar-btn navbar-right" onclick="Logout()" style="margin-right:25px">Logout</button>
  </div>
</nav>