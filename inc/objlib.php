<?php

    require_once('datamod.php');//db connection info
    
    require_once('lib_compat/csrf.php');//csrf token utilities
    
    class WebSample{
        
        private $_connUrl;
        
        private function DBConnect(&$_db_link, &$errorMsg){
            try {
                $_db_link = new PDO($this->_connUrl, DB_USER, DB_PASS);
            } catch (PDOException $e) {
                $errorMsg =  'Connection failed: ' . $e->getMessage();
                return false;
            }
            
            return true;    
        }
        
        public function __construct(){
            $this->_connUrl='mysql:dbname='.DB_DB.';host='.DB_IP;
        }
        
        public function Login($loginemail, $loginpassword, &$rtnMsg){
            try{
                if(!$this->DBConnect($pdo, $errorMsg)){
                    $rtnMsg='There was an error connecting to the database server';
                    
                    //log the errorMsg for review
                    
                    $return=array("result"=>"ERROR",
                    'responseMsg'=>$rtnMsg);
                    return $return;//exit now
                }//if error
                
    	        session_regenerate_id();//regenerate the sessionID to help prevent abuse
    	        
    	        $stmt = $pdo->prepare("select * from userlogins u inner join useractivations a on "
    	        ."u.userid = a.userid and a.userstatus=1 "
    	        ."where loginemail=:loginemail and loginpassword=:loginpass and deleted=0");
    	        
    	        if(!$stmt -> execute([":loginemail"=>$loginemail,":loginpass"=>$loginpassword])){
    	            $err = $stmt->errorInfo();
    	            //log this error
                    $rtnMsg = 'There was an error processing your login request';
                    $return=array("result"=>"ERROR",
                    'responseMsg'=>$rtnMsg);
                    return $return;//exit now
    	        }
    	        
    	        if($stmt->rowCount()==0){
    	            //user does not exist!
    	            $rtnMsg = 'Credentials were not recognized, please try again';
                    $return=array("result"=>"ERROR",
                    'responseMsg'=>$rtnMsg);
                    return $return;//exit now
    	        }else{
    	            //user exists
    	            
    	            $row = $stmt->fetch(PDO::FETCH_ASSOC);//we only want the first record
    	            
    	            $_SESSION['isloggedin']='yes';
    	            $_SESSION['userkey']=$row['userid'];
    	            
    	            $updateid = $row['logindid'];
    	            $stmt=null;//reset before querying the database again
    	            
    	            $stmt = $pdo->prepare('update userlogins set deleted=0 where loginid=:loginid');
    	            if(!$stmt -> execute([":loginid"=>$updateid])){
        	            //we don't REALLY need to do anything here, it's just nice to have the lastlogin updated for security/reference
        	        }
        	        
        	        $rtnMsg = 'Login Successful';
                    $return=array("result"=>"SUCCESS",
                        'responseMsg'=>$rtnMsg);
                        return $return;//exit now
    
        	        }
    	        
            }catch(Exception $e){
                //log this exception
                $rtnMsg = 'There was an error processing your login request';
                $return=array("result"=>"ERROR",
                'responseMsg'=>$rtnMsg);
                return $return;//exit now    
            }finally{
                if(isset($stmt)){$stmt=null;}
            }  
        }
        
        public function GetDemographics($userkey, &$rtnMsg){
            try{
                if(!$this->DBConnect($pdo, $errorMsg)){
                    $rtnMsg='There was an error connecting to the database server';
                    
                    //log the errorMsg for review
                    
                    $return=array("result"=>"ERROR",
                    'responseMsg'=>$rtnMsg);
                    return $return;//exit now
                }//if error
                
    	        session_regenerate_id();//regenerate the sessionID to help prevent abuse
    	        
    	        $stmt = $pdo->prepare("select * from userdemographics "
    	        ."where userid=:userid and deleted=0");
    	        
    	        if(!$stmt -> execute([":userid"=>$userkey])){
    	            $err = $stmt->errorInfo();
    	            //log this error, $err[2]
                    $rtnMsg = 'There was an error processing your request';
                    $return=array("result"=>"ERROR",
                    'responseMsg'=>$rtnMsg);
                    return $return;//exit now
    	        }
    	        
    	        if($stmt->rowCount()==0){
    	            //user does not exist!
    	            $rtnMsg = 'User key matches no know records';
                    $return=array("result"=>"ERROR",
                    'responseMsg'=>$rtnMsg);
                    return $return;//exit now
    	        }else{
    	            //user exists
    	            
    	            $row = $stmt->fetch(PDO::FETCH_ASSOC);//we only want the first record
    	            
    	            $rtnMsg='SUCCESS';
    	            
    	            $return=array("result"=>"SUCCESS",
                    'responseMsg'=>$rtnMsg);
                    
                    $return['demographics']=$row;
                    
                    $stmt=null;
                    return $return;
    	            
    	        }
    	        
            }catch(Exception $e){
                //log this exception
                $rtnMsg = 'There was an error processing your request';
                $return=array("result"=>"ERROR",
                'responseMsg'=>$rtnMsg);
                return $return;//exit now    
            }finally{
                if(isset($stmt)){$stmt=null;}
            }    
        }
        
        public function UpdateDemographics($userid, $firstname, $lastname, $email, $dob, $address, $city, $zip, $country, &$rtnMsg){
            try{
                if(!$this->DBConnect($pdo, $errorMsg)){
                    $rtnMsg='There was an error connecting to the database server';
                    
                    //log the errorMsg for review
                    
                    $return=array("result"=>"ERROR",
                    'responseMsg'=>$rtnMsg);
                    return $return;//exit now
                }//if error
                
    	        session_regenerate_id();//regenerate the sessionID to help prevent abuse
    	        
    	        $stmt=$pdo->prepare('update userdemographics set firstname=:firstname, lastname=:lastname, '
    	        .'email=:email, dob=:dob, address=:address, city=:city, zip=:zip, country=:country where userid=:userid');
    	        
    	        if(!$stmt -> execute([":firstname"=>$firstname,":lastname"=>$lastname,
    	        ":email"=>$email,":dob"=>$dob,":address"=>$address,":city"=>$city,
    	        ":zip"=>$zip,":country"=>$country,":userid"=>$userid,])){
    	            $err = $stmt->errorInfo();
    	            //log this error
                    $rtnMsg = 'There was an error processing your update request';
                    $return=array("result"=>"ERROR",
                    'responseMsg'=>$rtnMsg);
                    return $return;//exit now
    	        }else{
    	            $rtnMsg = 'SUCCESS';
                    $return=array("result"=>"SUCCESS",
                    'responseMsg'=>$rtnMsg);
                    return $return;//exit now    
    	        }
    	        
            }catch(Exception $e){
                //log this exception
                $rtnMsg = 'There was an error processing your update request';
                $return=array("result"=>"ERROR",
                'responseMsg'=>$rtnMsg);
                return $return;//exit now    
            }finally{
                if(isset($stmt)){$stmt=null;}
            } 
        }
    }//class WebSample
    
?>