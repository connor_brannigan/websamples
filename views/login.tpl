<div class="container login_panel">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Login</h3>
        </div>
        <div style="padding-top:30px" class="panel-body">
           
            <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="loginemail" type="text" class="form-control" name="username" value="" placeholder="E-Mail"  autocomplete="off">                                        
            </div>
                
            <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input id="loginpassword" type="password" class="form-control" name="password" placeholder="Password"  autocomplete="off">
            </div> 
            
            <div class="form-group" id="login_process">
                <div class="col-sm-12 controls">
                    <input type="button" class="btn btn-primary panel_login_button pull-right" value="SIGN IN" onclick="processLogin('loginProcess');" /> 
                </div>
            </div>
            {$logintoken}
            <div class="clear"><br/><br/></div>
            <div id="login_message_container" class="panel_feedback" >
                <span id="login_message"></span>
            </div>
            <div class="clear"><br/><br/></div>
            <div>
                <span>Demo Email: connor.brannigan@gmail.com</span><br/>
                <span>Demo Pass: WebSampl3</span>
            </div>
        </div>
    </div>
</div>