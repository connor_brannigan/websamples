<footer id="page-footer"class="footer">
    <div id="footer_container" class="container">
        <span class="footer-copyright">Connor Brannigan  &copy; {'Y'|date}</span><br />
    </div>
</footer>